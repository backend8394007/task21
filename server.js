const http = require('http');
const fs = require('fs');
const path = require("path");

http.createServer((req, res) =>{
   console.log(`Запрошенный адрес: ${req.url}`);
   if (req.url.startsWith('/public/')){
      const filePath = req.url.substr(1);
      let extname = path.extname(filePath);
      fs.readFile(filePath, (error, data) =>{
         if (error) {
            res.statusCode = 404;
            res.end('Resource not found!');
         }
         switch (extname) {
            case '.html':
               res.setHeader(
                   'Content-Type',
                   'text/html'
               );
               res.end(data);
               break;
            case '.css':
               res.setHeader(
                   'Content-Type',
                   'text/css'
               );
               res.end(data);
               break;
            case '.jpg':
               res.setHeader(
                   'Content-Type',
                   'image/jpg'
               );
               res.end(data);
               break;
            default:
               res.setHeader(
                   'Content-Type',
                   'text/html'
               );
               res.end('404.html');
               break;
         }
      });
   } else {
      res.end('Not dirname "/public/name"!');
   }
}).listen(3000);